# Setup
1. Install vue-cli globally: `$ npm i -g @vue/cli`
2. To lint as I do, also install standard and babel-eslint: `$ npm i -g standard babel-eslint`
3. Now that all the global tools are setup, install the dependencies: `$ yarn`

## Running
You can run each of the versions from the root folder via the npm scripts.
In general: `yarn <flavor>` will run that particular flavor.
So, in the root folder: `yarn react` would run the react example, `yarn vue` the vue example, etc, etc
